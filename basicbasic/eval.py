import re
from .basiclogger import logger


def eval_basic(code):
    locals = dict(_line_number=1)
    lines = [""]

    def compile_from_basic(line, line_number):
        line = line.strip()

        # fix divisions
        # we are only handling integers so we need use integer divisions
        # this line will change every single slash `/` to a double slash `//`
        line = re.sub(r"(?<!/)/(?!/)", "//", line)

        line = re.sub(r"goto", "_line_number =", line, flags=re.IGNORECASE)

        if isevil(line):
            return ""
        if line.startswith("print"):
            return f"print({line[6:]})"
        if line.startswith("if"):

            m = re.match(
                r"if (?P<condition>.*?) then (?P<then>.*?)( else (?P<else>.*?))?$", line
            )
            if not m:
                logger.warn(f"ignore line: {line!r}")
                return ""

            ifclauses = m.groupdict()
            condition = compile_from_basic(ifclauses["condition"], line_number)
            then_c = compile_from_basic(ifclauses["then"], line_number)
            else_c = (
                compile_from_basic(ifclauses["else"], line_number)
                if ifclauses["else"]
                else "..."
            )
            return f"if ({condition}):\n    {then_c}\nelse:\n    {else_c}"

        # if line.startswith("if"):
        #     locals["_lastif"] += 1
        #     return f"if not ({line[3:]}): _line_number = _else{locals['_lastif']} or _endif{locals['_lastif']}"
        # if line.startswith("else"):
        #     locals[f"_else{locals['_lastif']}"] = line_number + 1
        #     return f"_line_number = _endif{locals['_lastif']}"
        # if line.startswith("endif"):
        #     locals[f"endif{locals['_lastif']}"] = line_number + 1
        #     return ""

        return line

    for line_number, line in enumerate(code.splitlines(), 1):
        if isevil(line):
            continue
        lines.append(compile_from_basic(line, line_number))

    while True:
        line_number = locals["_line_number"]
        logger.debug(f"   ..lo: {line_number}")
        try:
            line = lines[line_number]
        except IndexError:
            logger.debug("..bye")
            return
        execute_line(line, locals, line_number)
        fix_integers(locals)


def execute_line(line, locals, line_number):
    locals["_line_number"] += 1
    logger.debug(f"       : {line}")
    exec(line, {}, locals)


def fix_integers(locals):
    for k in locals.keys():
        locals[k] %= 100


def isevil(line):
    if any(map(lambda x: x in line, ["(", "[", ";", ":", ".", "def", "import", "#"])):
        return True
    return False


# code = """a = 2
# if a <= 2 then
# print a
# def foo(): return 42
# #else
# a = a + 1
# print a
# endif"""

# code = """
# s = 0
# """

# eval_basic(code)
