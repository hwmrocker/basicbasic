from logzero import setup_logger
import logging

logger = setup_logger(name="basic", level=logging.WARNING)
# logger.setLevel(logging.DEBUG)
