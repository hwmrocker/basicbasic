from .context import basicbasic
import pytest


@pytest.mark.parametrize(
    "code,stdout",
    [
        pytest.param("print 1", "1\n", id="print"),
        pytest.param("print 1 + 1", "2\n", id="addition"),
        pytest.param("a = 1\nprint a", "1\n", id="store variable"),
        pytest.param("a = 1\na = a + 1\nprint a", "2\n", id="change variable"),
        pytest.param("print 2 * 3", "6\n", id="multiplication"),
        pytest.param("print 3 - 2 ", "1\n", id="substraction"),
        pytest.param("print 3 / 2 ", "1\n", id="division"),
        pytest.param("print 3 % 2 ", "1\n", id="division"),
        pytest.param("if 1 then print 1", "1\n", id="if then"),
        pytest.param("if 0 then print 1", "", id="if not"),
        pytest.param("if 0 then print 1 else print 2", "2\n", id="if else"),
        pytest.param("a=1\ngoto 4\na=2\nprint a", "1\n", id="goto"),
        pytest.param("a=1\nif a then goto 4\na=2\nprint a", "1\n", id="goto in if"),
        pytest.param("a=0\nif a then goto 4\na=2\nprint a", "2\n", id="ignore goto"),
        pytest.param(
            "a=0\nif a then goto 9 else goto 4\ngoto 9\na=2\nprint a",
            "2\n",
            id="goto in else",
        ),
    ],
)
def test_eval(code, stdout, capsys):
    basicbasic.eval.eval_basic(code)
    captured = capsys.readouterr()
    assert captured.out == stdout
